#!/usr/bin/env python
# coding: utf-8

import sys
from setuptools import setup

if sys.version_info < (3, 8, 0):
    sys.stderr.write('ERROR: You need Python 3.8+ '
                     'to install the simulated_annealing package.\n')
    exit(1)

version = '0.1'
description = 'Simulated Annealing for Python 3.8+'
long_description = '''\
From [https://en.wikipedia.org/wiki/Simulated_annealing](Wikipedia):

> Simulated annealing (SA) is a probabilistic technique for approximating the
> global optimum of a given function.

Use this algorithm to find an approximate solution for certain
[https://en.wikipedia.org/wiki/NP-completeness](NP-complete)
problems like the
[https://en.wikipedia.org/wiki/Travelling_salesman_problem](Traveling salesman problem).
'''

classifiers = [
    'Development Status :: 3 - Alpha',
    'Environment :: Console',
    'Intended Audience :: Developers',
    'License :: OSI Approved :: GNU General Public License v3 or later (GPLv3+)',
    'Operating System :: OS Independent',
    'Programming Language :: Python :: 3.8',
    'Programming Language :: Python :: 3.9',
    'Topic :: Software Development',
]

setup(name='simulated_annealing',
      version=version,
      description=description,
      long_description=long_description,
      author='Eric Man',
      author_email='eric@dataflower.com.au',
      url='https://gitlab.com/eric-dataflower/simulated_annealing',
      license='GPLv3',
      keywords='simulated annealing algorithm np-complete',
      package_dir={'': 'src'},
      py_modules=['simulated_annealing'],
      classifiers=classifiers,
      install_requires=["python_version >= '3.8'"])
