# Simulated Annealing

## What is Simulated Annealing?

From [Wikipedia](https://en.wikipedia.org/wiki/Simulated_annealing):

> Simulated annealing (SA) is a probabilistic technique for approximating the
> global optimum of a given function.

## Install

`pip install git+https://gitlab.com/eric-dataflower/simulated_annealing.git#egg=simulated_annealing`

## Usage

See [test_simulated_annealing.py](src/test_simulated_annealing.py).

## License

This project is by default licensed under GPL v3. For other license terms,
contact the [author](mailto:eric@dataflower.com.au).
