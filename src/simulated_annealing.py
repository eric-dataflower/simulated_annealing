import math
import random
from typing import TypeVar, Tuple, Callable, Generator


def acceptance_probability(
        neighbor_cost: float,
        cost: float,
        temperature: float):

    if neighbor_cost < cost:
        return 1.0
    
    return math.exp(-(neighbor_cost - cost) / temperature)


State = TypeVar('State')


def _anneal(
    start_state: State,
    start_cost: float,
    get_neighbor_and_cost: Callable[[State, float], Tuple[State, float]],
    maximum_steps: int = 100000,
    get_temperature: Callable[[float, float], float]
        = lambda a, b: math.sqrt(1.0 - (a/b))) \
        -> Generator[
            Tuple[int, float, State, float], None, Tuple[State, float]]:

    state = start_state
    cost = start_cost
    best_state, best_cost = start_state, start_cost

    for k in range(0, maximum_steps):
        temperature = get_temperature(k, maximum_steps)
        if temperature < 1e-8:
            break

        neighbor, neighbor_cost = get_neighbor_and_cost(state, cost)

        probability = acceptance_probability(neighbor_cost, cost, temperature)

        if probability >= random.random():
            state = neighbor
            cost = neighbor_cost
        
        if cost < best_cost:
            best_state, best_cost = state, cost
        
        yield k, temperature, state, cost

    return best_state, best_cost


def anneal(
        *args,
        maximum_steps: int = 100000,
        every: int = 0,
        **kwargs
    ):
    anneal_instance = _anneal(*args, maximum_steps=maximum_steps, **kwargs)
    try:
        while True:
            step, temperature, state, cost = next(anneal_instance)
            if every and step % every == 0:
                yield step, state, cost
    except StopIteration as e:
        return maximum_steps, *e.value
