from .simulated_annealing import anneal
from typing import List, Tuple, Callable
import math
import random

coords_txt = """
1 0 0
2 3 5
3 2.5 9
4 48 16
5 48 17
6 69 16
7 70 16
8 64 13
9 3 -22
10 2.5 1
11 -13 11.5
12 -20 45
13 -9 52
14 -8.5 53
15 -8 52
16 2 2
17 -2 9
18 -10 20
19 -20 19
20 -15 -21
21 -5 -9
22 -4.5 -9
23 -52 -36
24 -53 -36
25 0 0.01
26 -30 -18
27 -51 -35
28 81 9
29 84 -99
30 82 -6
31 40 -12
32 50 -7
33 51 -8
34 63 -17
35 45 -1
36 54 8.5
37 29 4
38 21 3
39 22 2
40 39 -3
41 39.5 -3
42 40 -11
43 28 -2
44 24 -18
45 24 -19
"""

data = {}
state = []

for line in coords_txt.split("\n"):
    if line:
        i, x, y = line.split()
        data[int(i)] = (float(x), float(y))
        state.append(int(i))


def get_cost(state: List[int]) -> float:
    distance = 0.0
    j = None
    for i in state:
        if j:
            x1, y1 = data[i]
            x2, y2 = data[j]
            distance += math.sqrt(
                (x2 - x1) ** 2 + (y2 - y1) ** 2
            )
        j = i
    return distance


def get_neighbor_and_cost(state: List[int], cost: float) -> Tuple[List[int], float]:
    a = random.randint(0, len(state) - 1)
    b = a
    while b == a:
        b = random.randint(0, len(state) - 1)
    neighbor = state.copy()
    neighbor[b], neighbor[a] = neighbor[a], neighbor[b]
    return neighbor, get_cost(neighbor)


def solve_tsp(every: int, maximum_steps: int):
    global state
    start_state = state.copy()
    start_cost = get_cost(state)

    yield (yield from anneal(
        start_state,
        start_cost,
        get_neighbor_and_cost,
        maximum_steps=maximum_steps,
        get_temperature=lambda a, b: math.sqrt(1.0 - (a / b)) * 20.0,
        every=every))

def test_anneal():
    progress = []

    maximum_steps = 10000
    every = 1000

    for step, state, cost in solve_tsp(every=every,
            maximum_steps=maximum_steps):
        progress.append((step, cost))

    assert len(progress) == maximum_steps / every + 1


def solve_sort():
    """
    Reverse sort a list of numbers, using simulated annealing.
    """

    def _get_cost(state):
        previous = state[0]
        cost = 0.0
        for value in state[1:]:
            if value > previous:
                cost += value - previous
            previous = value
        return cost

    def _get_neighbor_and_cost(state, cost):
        a = random.randint(0, len(state) - 1)
        b = random.randint(0, len(state) - 1)
        neighbor = state.copy()
        neighbor[b], neighbor[a] = neighbor[a], neighbor[b]
        return neighbor, _get_cost(neighbor)


    start_state = [8, 3, 2, 4, 5, 7, 6, 10, 9, 1]
    start_cost = _get_cost(start_state)

    yield (yield from anneal(
        start_state,
        start_cost,
        _get_neighbor_and_cost,
        maximum_steps=10000,
        get_temperature=lambda a, b: 1.0))

def test_sort():
    # Given the large number of maximum steps, a simple sorting problem
    # ought to always be solved optimally.
    step, state, cost = next(solve_sort())
    assert state == [10, 9, 8, 7, 6, 5, 4, 3, 2, 1]
